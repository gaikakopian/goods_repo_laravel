<?php namespace Api\Common\Models;


use Illuminate\Database\Eloquent\Model;


/**
 * Class ApiKey
 *
 * @package    Api\Common\Auth\Models;
 * @subpackage ApiKey
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class Category extends Model
{
    protected $fillable = [
        'title',
        'description'
    ];

}