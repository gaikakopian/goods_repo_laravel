<?php namespace Api\Common\Models;


use Illuminate\Database\Eloquent\Model;


class Good extends Model
{
    protected $fillable = [
        'category_id',
        'user_id',
        'title',
        'price',
        'description',
        'photo',
        'photo_description'
    ];

}