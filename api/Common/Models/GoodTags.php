<?php namespace Api\Common\Models;

use Api\V1\General\Models\UGood;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;


/**
 * Class ApiKey
 *
 * @package    Api\Common\Auth\Models;
 * @subpackage ApiKey
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class GoodTags extends Model
{
    protected $fillable = [
        'good_id',
        'tag_id'
    ];

    public function good() :HasOne{
        return $this->hasOne(UGood::class , 'id','good_id');
    }

}