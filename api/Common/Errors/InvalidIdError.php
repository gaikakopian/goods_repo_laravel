<?php
namespace Api\Common\Errors;

/**
 * File InvalidIdError.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\Common\Errors
 * @subpackage OtherApplicationError.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

/**
 * Class InvalidIdError
 *
 * Generate additional message when InvalidIdException is thrown
 *
 * @package   Api\Common\Errors;
 * @subpackage OtherApplicationError
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class InvalidIdError extends Error
{
    /**
     * @const int
     */
    const CODE = 400;

    /**
     * @const string
     */
    const MESSAGE = 'Invalid Id: ';
}