<?php namespace Api\Common\Errors;

/**
 * File CurlError.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\Common\Errors
 * @subpackage OtherApplicationError.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

/**
 * Class CurlError
 *
 * Generate additional message when CurlException is thrown
 *
 * @package    Api\Common\Errors;
 * @subpackage OtherApplicationError
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class CurlError extends Error
{
    /**
     * @const int
     */
    const CODE = 2000;

    /**
     * @const string
     */
    const MESSAGE = 'Curl Error: ';
}