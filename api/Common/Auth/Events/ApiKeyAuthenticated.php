<?php namespace Api\Common\Auth\Events;

/**
 * File ApiKeyAuthenticated.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\Common\Auth\Events
 * @subpackage ApiKeyAuthenticated.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;
use Api\Common\Auth\Models\ApiKey;

/**
 * Class ApiKeyAuthenticated
 *
 * @package    Api\Common\Auth\Events;
 * @subpackage ApiKeyAuthenticated
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class ApiKeyAuthenticated
{
    use SerializesModels;

    public $request;

    public $apiKey;

    /**
     * Create a new event instance.
     *
     * @param Request $request
     * @param ApiKey  $apiKey
     */
    public function __construct(Request $request, ApiKey $apiKey)
    {
        $this->request = $request;
        $this->apiKey  = $apiKey;
    }
}