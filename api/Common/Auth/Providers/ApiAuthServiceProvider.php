<?php namespace Api\Common\Auth\Providers;

/**
 * File ApiAuthServiceProvider.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\Common\Auth\Providers
 * @subpackage ApiAuthServiceProvider.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\Auth\Http\Middleware\AuthenticateGeneralApiKey;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Api\Common\Auth\Console\Commands\GenerateApiKey;
use Api\Common\Auth\Http\Middleware\AuthenticateApiKey;

/**
 * Class ApiAuthServiceProvider
 *
 * @package    Api\Common\Auth\Providers;
 * @subpackage ApiAuthServiceProvider
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class ApiAuthServiceProvider extends ServiceProvider
{
    protected $middlewares = [
        'auth.apikey' => AuthenticateApiKey::class,
        'auth.general' => AuthenticateGeneralApiKey::class
    ];

    /**
     * Bootstrap the application services.
     *
     * @param Router $router
     *
     * @return void
     */
    public function boot(Router $router)
    {
        // Publish migrations
        $this->publishMigrations();
        $this->defineMiddleware($router);
    }

    /**
     * Publish Migration files from library to Base of Laravel
     *
     * @author     Gaik Akopian <gaikakopian94@gmail.com>
     *
     * @return void
     */
    private function publishMigrations()
    {
        $this->publishes([
            __DIR__ . '/../../../database/migrations/' => base_path('/database/migrations'),
        ], 'migrations');
    }

    /**
     * @param Router $router
     *
     * @author     Gaik Akopian <gaikakopian94@gmail.com>
     *
     * @return void
     */
    private function defineMiddleware(Router $router)
    {
        foreach ($this->middlewares as $name => $class)
        {
            if (version_compare(app()->version(), '5.4.0') >= 0)
            {
                dd('aaa');
                $router->aliasMiddleware($name, $class);
            }
            else
            {
                dd('bbb');
                $router->middleware($name, $class);
            }
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->commands([
            GenerateApiKey::class,
        ]);
    }
}