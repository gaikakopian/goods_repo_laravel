<?php namespace Api\Common;

/**
 * File Controller.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\Common
 * @subpackage Controller.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

/**
 * Class Controller
 *
 * @package    Api\Common;
 * @subpackage Controller
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class ApiController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @var Response
     */
    protected $response;

    /**
     * ApiController constructor.
     */
    public function __construct()
    {
        $this->response = new Response(null, null, null);
    }
}