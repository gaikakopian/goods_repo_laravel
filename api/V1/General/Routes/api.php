<?php

use Api\V1\General\Middlewares\CheckCategoryExist;
use Api\V1\General\Middlewares\CheckCanUserUpdate;
use Api\V1\General\Middlewares\CheckTagOnCreate;
use Api\V1\General\Middlewares\CheckTagOnDelete;



Route::prefix('general')->group(function () {

    Route::post('/register' , 'RegistrationController@register');
    Route::post('/login-email' , 'LoginController@loginEmail');
    Route::post('/login-login' , 'LoginController@loginLogin');

    Route::middleware(['auth.apikey'])->group(function () {

        //Category requests
        Route::get('/categories/list', 'CategoriesController@index');
        Route::get('/categories/{id}', 'CategoriesController@show');
        Route::post('/categories/create' , 'CategoriesController@store');
        Route::put('/categories/update/{id}', 'CategoriesController@update');
        Route::delete('/categories/delete/{id}', 'CategoriesController@destroy');

        //Tags requests
        Route::get('/tags/list', 'TagsController@index');
        Route::get('/tags/{id}', 'TagsController@show');
        Route::post('/tags/create' , 'TagsController@store');
        Route::put('/tags/update/{id}', 'TagsController@update');
        Route::delete('/tags/delete/{id}', 'TagsController@destroy');

        //Goods requests
        Route::get('/goods/list', 'GoodsController@index');
        Route::get('/goods/tag/{id}', 'GoodsController@getByTag');
        Route::get('/goods/{id}', 'GoodsController@show');
        Route::post('/goods/create' , 'GoodsController@store')->middleware([CheckCategoryExist::class]);
        Route::put('/goods/update/{id}', 'GoodsController@update')->middleware([CheckCanUserUpdate::class]);
        Route::delete('/goods/delete/{id}', 'GoodsController@destroy')->middleware([CheckCanUserUpdate::class]);

        //Good Tags requests
        Route::post('/good-tags/create' , 'GoodTagsController@store')->middleware([CheckTagOnCreate::class]);
        Route::delete('/good-tags/delete/{id}', 'GoodTagsController@destroy')->middleware([CheckTagOnDelete::class]);

        //Logout
        Route::get('/logout' , 'LoginController@logout');

    });
});
