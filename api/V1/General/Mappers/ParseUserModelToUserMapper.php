<?php
namespace Api\V1\General\Mappers;

/**
 * File UserModelToUserMapper.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\SmartMailer\Mappers
 * @subpackage UserModelToUserMapper.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\V1\General\Entities\UserEntity;

/**
 * Class UserModelToUserMapper
 *
 * Parse the Model object to entity
 *
 * @package    Api\V1\General\Mappers;
 * @subpackage UserModelToUserMapper
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class ParseUserModelToUserMapper
{
    /**
     * Map the model to entity
     *
     * @param      $user
     *
     * @author     Gaik Akopian <gaikakopian94@gmail.com>
     *
     * @return UserEntity
     */
    public static function map($user)
    {
        return new UserEntity([
            'userId'    => $user->id,
            'email'     => $user->email,
            'firstName' => $user->first_name,
            'lastName'  => $user->last_name,
            'username'  => $user->username,
            'accountId' => $user->account_id,
            'isAdmin' => $user->is_admin,
            'picture' => $user->profile_pic ?? '',
            'createdAt' => $user->created_at,
        ]);
    }

}