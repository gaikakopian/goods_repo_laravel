<?php
namespace Api\V1\General\Controllers;

/**
 * File UserController.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\General\Controllers
 * @subpackage UserController.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\ApiController;
use Api\Common\Errors\ErrorPrefix;
use Api\Common\Helpers\HttpCode;
use Api\Common\Exceptions\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Api\V1\General\Services\UserService;

/**
 * Class UserController
 *
 * @package    Api\V1\General\Controllers;
 * @subpackage UserController
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class UserController extends ApiController
{

    protected $userService;

    /**
     * UserController constructor.
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->userService = new UserService();
    }

    public function auth(Request $request){
        try {
            $user = $this->userService->auth($request);
            $result     = $this->response->setSuccess($user);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::INTERNAL_SERVER_ERROR;
            $result     = $this->response->setErrorFromException(ErrorPrefix::GENERAL, $e);
        }

        return Response::json($result, $httpStatus);
    }

}