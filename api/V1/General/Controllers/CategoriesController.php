<?php
namespace Api\V1\General\Controllers;

/**
 * File SalonsController.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\General\Controllers
 * @subpackage SalonsController.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\ApiController;
use Api\Common\Errors\ErrorPrefix;
use Api\Common\Helpers\HttpCode;
use Api\Common\Exceptions\Exception;
use Api\V1\General\Services\CategoriesService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Api\Common\Exceptions\InvalidIdException;

/**
 * Class SalonsController
 *
 * @package    Api\V1\General\Controllers;
 * @subpackage SalonsController
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class CategoriesController extends ApiController
{

    protected $categoriesService;

    /**
     * UserController constructor.
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->categoriesService = new CategoriesService();
    }

    public function index(Request $request)
    {
        try {
            $lists = $this->categoriesService->getAllCategories($request);
            $result     = $this->response->setSuccess($lists);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::FORBIDDEN;
            $result     = $this->response->setErrorFromException(ErrorPrefix::SMART_MAILER, $e);
        }

        return Response::json($result, $httpStatus);
    }

    public function store(Request $request){

        try {
            $result     = $this->categoriesService->store($request->all());
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::INTERNAL_SERVER_ERROR;
            $result     = $this->response->setErrorFromException(ErrorPrefix::GENERAL, $e);
        }

        return Response::json($result, $httpStatus);

    }

    public function update(Request $request)
    {

        try {
            $list = $this->categoriesService->update( $request->id, $request->all());
            $result     = $this->response->setSuccess($list);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::FORBIDDEN;
            $result     = $this->response->setErrorFromException(ErrorPrefix::SMART_MAILER, $e);
        }

        return Response::json($result, $httpStatus);
    }

    public function show(Request $request)
    {
        try {
            if (!is_numeric($request->id)){
                throw new InvalidIdException();
            }
            $list = $this->categoriesService->getCategory($request->id);
            $result     = $this->response->setSuccess($list);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::FORBIDDEN;
            $result     = $this->response->setErrorFromException(ErrorPrefix::SMART_MAILER, $e);
        }

        return Response::json($result, $httpStatus);
    }

    public function destroy(Request $request)
    {
        try {
            if (!is_numeric($request->id)){
                throw new InvalidIdException();
            }
            $list = $this->categoriesService->delete($request->id);
            $result     = $this->response->setSuccess($list);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::FORBIDDEN;
            $result     = $this->response->setErrorFromException('', $e);
        }

        return Response::json($result, $httpStatus);
    }

}