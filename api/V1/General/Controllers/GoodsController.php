<?php
namespace Api\V1\General\Controllers;

/**
 * File SalonsController.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\General\Controllers
 * @subpackage SalonsController.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\ApiController;
use Api\Common\Errors\ErrorPrefix;
use Api\Common\Helpers\HttpCode;
use Api\Common\Exceptions\Exception;
use Api\V1\General\Services\GoodsService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Api\Common\Exceptions\InvalidIdException;

/**
 * Class SalonsController
 *
 * @package    Api\V1\General\Controllers;
 * @subpackage SalonsController
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class GoodsController extends ApiController
{

    protected $goodsService;

    /**
     * UserController constructor.
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->goodsService = new GoodsService();
    }

    public function index(Request $request)
    {
        try {
            $lists = $this->goodsService->getAllGoods($request);
            $result     = $this->response->setSuccess($lists);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::FORBIDDEN;
            $result     = $this->response->setErrorFromException(ErrorPrefix::SMART_MAILER, $e);
        }

        return Response::json($result, $httpStatus);
    }

    public function store(Request $request){

        try {
            $result     = $this->goodsService->store($request->all(),$request->apiKey);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::INTERNAL_SERVER_ERROR;
            $result     = $this->response->setErrorFromException(ErrorPrefix::GENERAL, $e);
        }

        return Response::json($result, $httpStatus);

    }

    public function update(Request $request)
    {
        try {
            $list = $this->goodsService->update( $request->id, $request->all(),$request->apiKey);
            $result     = $this->response->setSuccess($list);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::FORBIDDEN;
            $result     = $this->response->setErrorFromException(ErrorPrefix::SMART_MAILER, $e);
        }

        return Response::json($result, $httpStatus);
    }


    public function show(Request $request)
    {
        try {
            if (!is_numeric($request->id)){
                throw new InvalidIdException();
            }
            $list = $this->goodsService->getGood($request->id);
            $result  = $this->response->setSuccess($list);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::FORBIDDEN;
            $result     = $this->response->setErrorFromException(ErrorPrefix::SMART_MAILER, $e);
        }

        return Response::json($result, $httpStatus);
    }


    public function getByTag(Request $request)
    {
        try {
            if (!is_numeric($request->id)){
                throw new InvalidIdException();
            }
            $list = $this->goodsService->getByTag($request->id);
            $result  = $this->response->setSuccess($list);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::FORBIDDEN;
            $result     = $this->response->setErrorFromException(ErrorPrefix::SMART_MAILER, $e);
        }

        return Response::json($result, $httpStatus);
    }

    public function destroy(Request $request)
    {
        try {
            if (!is_numeric($request->id)){
                throw new InvalidIdException();
            }
            $list = $this->goodsService->delete($request->id);
            $result     = $this->response->setSuccess($list);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::FORBIDDEN;
            $result     = $this->response->setErrorFromException('', $e);
        }

        return Response::json($result, $httpStatus);
    }

}