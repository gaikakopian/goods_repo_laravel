<?php
namespace Api\V1\General\Controllers;

/**
 * File SalonsController.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\General\Controllers
 * @subpackage SalonsController.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\ApiController;
use Api\Common\Errors\ErrorPrefix;
use Api\Common\Helpers\HttpCode;
use Api\Common\Exceptions\Exception;
use Api\V1\General\Services\CategoriesService;
use Api\V1\General\Services\GoodTagsService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Api\V1\General\Services\SalonsService;
use Api\Common\Exceptions\InvalidIdException;

/**
 * Class SalonsController
 *
 * @package    Api\V1\General\Controllers;
 * @subpackage SalonsController
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class GoodTagsController extends ApiController
{

    protected $goodTagsService;

    /**
     * UserController constructor.
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->goodTagsService = new GoodTagsService();
    }


    public function store(Request $request){

        try {
            $result     = $this->goodTagsService->store($request->all());
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::INTERNAL_SERVER_ERROR;
            $result     = $this->response->setErrorFromException(ErrorPrefix::GENERAL, $e);
        }

        return Response::json($result, $httpStatus);

    }

    public function destroy(Request $request)
    {
        try {
            if (!is_numeric($request->id)){
                throw new InvalidIdException();
            }
            $list = $this->goodTagsService->delete($request->id);
            $result     = $this->response->setSuccess($list);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::FORBIDDEN;
            $result     = $this->response->setErrorFromException('', $e);
        }

        return Response::json($result, $httpStatus);
    }

}