<?php
namespace Api\V1\General\Controllers;

/**
 * File SalonsController.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\General\Controllers
 * @subpackage SalonsController.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\ApiController;
use Api\Common\Errors\ErrorPrefix;
use Api\Common\Helpers\HttpCode;
use Api\Common\Exceptions\Exception;
use Api\V1\General\Services\CategoriesService;
use Api\V1\General\Services\TagsService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Api\V1\General\Services\SalonsService;
use Api\Common\Exceptions\InvalidIdException;

/**
 * Class SalonsController
 *
 * @package    Api\V1\General\Controllers;
 * @subpackage SalonsController
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class TagsController extends ApiController
{

    protected $tagsService;

    /**
     * UserController constructor.
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->tagsService = new TagsService();
    }

    public function index(Request $request)
    {
        try {
            $lists = $this->tagsService->getAllTags($request);
            $result     = $this->response->setSuccess($lists);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::FORBIDDEN;
            $result     = $this->response->setErrorFromException(ErrorPrefix::SMART_MAILER, $e);
        }

        return Response::json($result, $httpStatus);
    }

    public function store(Request $request){

        try {
            $result     = $this->tagsService->store($request->all());
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::INTERNAL_SERVER_ERROR;
            $result     = $this->response->setErrorFromException(ErrorPrefix::GENERAL, $e);
        }

        return Response::json($result, $httpStatus);

    }

    public function update(Request $request)
    {

        try {
            $list = $this->tagsService->update( $request->id, $request->all());
            $result     = $this->response->setSuccess($list);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::FORBIDDEN;
            $result     = $this->response->setErrorFromException(ErrorPrefix::SMART_MAILER, $e);
        }

        return Response::json($result, $httpStatus);
    }

    public function show(Request $request)
    {
        try {
            if (!is_numeric($request->id)){
                throw new InvalidIdException();
            }
            $list = $this->tagsService->getTag($request->id);
            $result     = $this->response->setSuccess($list);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::FORBIDDEN;
            $result     = $this->response->setErrorFromException(ErrorPrefix::SMART_MAILER, $e);
        }

        return Response::json($result, $httpStatus);
    }

    public function destroy(Request $request)
    {
        try {
            if (!is_numeric($request->id)){
                throw new InvalidIdException();
            }
            $list = $this->tagsService->delete($request->id);
            $result     = $this->response->setSuccess($list);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::FORBIDDEN;
            $result     = $this->response->setErrorFromException('', $e);
        }

        return Response::json($result, $httpStatus);
    }

}