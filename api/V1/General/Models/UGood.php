<?php namespace Api\V1\General\Models;

use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
/**
 * File SalonAgendas.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @subpackage SalonAgendas.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\Models\Good;


/**
 * Class SalonAgendas
 *
 * @package    Api\Common\Auth\Models;
 * @subpackage ApiKey
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class UGood extends Good
{
    protected $table = 'goods';

    public static function getGood($id){
        $good = self::with('category','tags')
            ->where('id', $id)
            ->first();

        return $good;
    }

    public static function getGoods(){
        $goods = self::with('category','tags')
            ->orderBy('created_at', 'desc')
            ->get();

        return $goods;
    }

    public function tags() :HasMany{
        return $this->hasMany(UGoodTags::class , 'good_id')->with('tag');
    }

    public function category() :HasOne{
        return $this->hasOne(UCategory::class , 'id','category_id');
    }
}