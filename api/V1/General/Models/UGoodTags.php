<?php namespace Api\V1\General\Models;

use Illuminate\Database\Eloquent\Relations\HasOne;
/**
 * File SalonAgendas.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @subpackage SalonAgendas.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\Models\GoodTags;


/**
 * Class SalonAgendas
 *
 * @package    Api\Common\Auth\Models;
 * @subpackage ApiKey
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class UGoodTags extends GoodTags
{
    protected $table = 'good_tags';

    public function tag() :HasOne{
        return $this->hasOne(UTag::class , 'id','tag_id');
    }
}