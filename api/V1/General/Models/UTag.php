<?php namespace Api\V1\General\Models;


/**
 * File SalonAgendas.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @subpackage SalonAgendas.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\Models\Category;
use Api\Common\Models\GoodTags;
use Api\Common\Models\SalonPortfolio;
use Api\Common\Models\Tags;
use Illuminate\Database\Eloquent\Relations\HasMany;


/**
 * Class SalonAgendas
 *
 * @package    Api\Common\Auth\Models;
 * @subpackage ApiKey
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class UTag extends Tags
{
    protected $table = 'tags';

    public static function getByTag($id){
        $tag = self::with('tags')
            ->where('id', $id)
            ->first();

        return $tag;
    }

    public function tags() :HasMany{
        return $this->hasMany(GoodTags::class , 'tag_id')->with('good');
    }
}