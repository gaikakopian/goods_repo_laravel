<?php
namespace Api\V1\General\Services;

/**
 * File UserService.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\General\Services
 * @su1bpackage RegisterService.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\V1\General\Exceptions\InvalidUserRegistrationException;
use Api\V1\General\Models\SSalonAdmin;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Api\Common\Exceptions\Exception;

use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;

/**
 * Class UserService
 *
 * Perform business operations for User
 *
 * @package    Api\V1\General\Services;
 * @subpackage UserService
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class RegisterService extends Service
{

    use RegistersUsers;

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'login' => 'required|string|max:255|unique:users',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'login' => $data['login'],
            'password' => Hash::make($data['password']),
        ]);
    }

    /**
     * Registers new user.
     *
     * @param  array  $data
     * @return \App\User
     */
    public function register($request){

        if( count($this->validator($request)->messages()) ){
            throw new InvalidUserRegistrationException($this->validator($request)->messages());
        }else{
            try{
                return $this->create($request);
            } catch (\Exception $e) {
                throw new Exception($e->getMessage());
            }
        }
    }
}