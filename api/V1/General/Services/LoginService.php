<?php
namespace Api\V1\General\Services;

/**
 * File UserService.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\General\Services
 * @su1bpackage RegisterService.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\V1\General\Exceptions\InvalidUserRegistrationException;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Auth;
use App\User;
use Api\Common\Auth\Models\ApiKey;
use Api\V1\General\Exceptions\NoUserFoundWithUsernameException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

use Illuminate\Foundation\Auth\AuthenticatesUsers;

/**
 * Class UserService
 *
 * Perform business operations for User
 *
 * @package    Api\V1\General\Services;
 * @subpackage UserService
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class LoginService extends Service
{
    use AuthenticatesUsers,ValidatesRequests;

    public function start($request){
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            $user = User::whereEmail($request->email)->first();
            $apikey = ApiKey::make($user,'user signup');
            return $apikey->key;
        }else{
            throw new InvalidUserRegistrationException();
        }
    }

    public function loginLogin($request){
        if(Auth::attempt(['login' => $request->login, 'password' => $request->password])){
            $user = User::where('login',$request->login)->first();
            $apikey = ApiKey::make($user,'user signup');
            return $apikey->key;
        }else{
            throw new InvalidUserRegistrationException();
        }
    }

    public function logout($request){

        $apikey = ApiKey::find($request->apiKey->id);
        if(!$apikey) throw new NotFoundException();
        $apikey->delete();
        return true;
    }
}