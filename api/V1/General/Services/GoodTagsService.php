<?php
namespace Api\V1\General\Services;

/**
 * File SalonService.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\General\Services
 * @su1bpackage SalonService.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\V1\General\Exceptions\InvalidUserRegistrationException;
use Api\V1\General\Models\SMaster;
use Api\V1\General\Models\UCategory;
use Api\V1\General\Models\UGoodTags;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Api\Common\Exceptions\NotFoundException;
use Api\V1\General\Models\SSalon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

/**
 * Class SalonService
 *
 * Perform business operations for User
 *
 * @package    Api\V1\General\Services;
 * @subpackage SalonService
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class GoodTagsService extends Service
{
    use AuthenticatesUsers,ValidatesRequests;

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Salons
     */
    protected function create(array $data)
    {
        $good_tag = UGoodTags::create([
            'tag_id' => $data['tag_id'],
            'good_id' => $data['good_id']
        ]);
        return $good_tag;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'tag_id' => 'required',
            'good_id' => 'required',
        ]);
    }

    public function store($request){

        if( count($this->validator($request)->messages()) ){
            throw new InvalidUserRegistrationException($this->validator($request)->messages());
        }else{
            return $this->create($request);
        }
    }

    public function delete($id)
    {
        /**
         * Fetch salon by user id and list id
         */
        try {
            $good_tag  = UGoodTags::where('id' , $id)
                ->first();
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
        /**
         * if not records found throw exception
         */
        if (is_null($good_tag)){
            throw new NotFoundException();
        }

        /**
         * Delete list from db
         */
        try{
            DB::transaction(function () use ($good_tag) {
                $good_tag->delete();
            });
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }

        return true;
    }
}