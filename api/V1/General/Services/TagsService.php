<?php
namespace Api\V1\General\Services;

/**
 * File SalonService.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\General\Services
 * @su1bpackage SalonService.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\V1\General\Exceptions\InvalidUserRegistrationException;
use Api\V1\General\Models\SMaster;
use Api\V1\General\Models\UCategory;
use Api\V1\General\Models\UTag;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Api\Common\Exceptions\NotFoundException;
use Api\V1\General\Models\SSalon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

/**
 * Class SalonService
 *
 * Perform business operations for User
 *
 * @package    Api\V1\General\Services;
 * @subpackage SalonService
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class TagsService extends Service
{
    use AuthenticatesUsers,ValidatesRequests;

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Salons
     */
    protected function create(array $data)
    {
        $tag = UTag::create([
            'title' => $data['title'],
            'description' => $data['description']
        ]);
        return $tag;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'title' => 'required|string|max:255'
        ]);
    }

    public function getAllTags($request)
    {
        /**
         * Fetch lists by user id
         */
        try {
            $tags = UTag::all();
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }

        return $tags;
    }

    public function store($request){

        if( count($this->validator($request)->messages()) ){
            throw new InvalidUserRegistrationException($this->validator($request)->messages());
        }else{
            return $this->create($request);
        }
    }

    public function update($id, $data){

        $tag = UTag::find($id);

        if(!$tag) throw new NotFoundException();

        return $tag->update($data);

    }

    public function getTag($id)
    {
        /**
         * Fetch lists by user id and list id
         */
        try {
            $tag = UCategory::find($id);
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }

        /**
         * if no records found throw exception
         */
        if (is_null($tag)){
            throw new NotFoundException();
        }

        return $tag;
    }

    public function delete($id)
    {
        /**
         * Fetch salon by user id and list id
         */
        try {
            $tag  = UTag::where('id' , $id)
                ->first();
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
        /**
         * if not records found throw exception
         */
        if (is_null($tag)){
            throw new NotFoundException();
        }

        /**
         * Delete list from db
         */
        try{
            DB::transaction(function () use ($tag) {
                $tag->delete();
            });
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }

        return true;
    }
}