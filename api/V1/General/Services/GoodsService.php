<?php
namespace Api\V1\General\Services;

/**
 * File SalonService.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\General\Services
 * @su1bpackage SalonService.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\V1\General\Exceptions\InvalidUserRegistrationException;
use Api\V1\General\Mappers\ParseGoodModelToGooMapper;
use Api\V1\General\Models\UCategory;
use Api\V1\General\Models\UGood;
use Api\V1\General\Models\UTag;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Api\Common\Exceptions\NotFoundException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

/**
 * Class SalonService
 *
 * Perform business operations for User
 *
 * @package    Api\V1\General\Services;
 * @subpackage SalonService
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class GoodsService extends Service
{
    use AuthenticatesUsers,ValidatesRequests;

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Salons
     */
    protected function create(array $data, $user_id)
    {
        $good = UGood::create([
            'category_id' => $data['category_id'],
            'user_id' => $user_id,
            'title' => $data['title'],
            'price' => $data['price'],
            'description' => $data['description'],
            'photo' => $data['photo'],
            'photo_description' => $data['photo_description'],
        ]);
        return $good;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'category_id' => 'required',
            'title' => 'required',
            'price' => 'required',
            'file' => 'image|mimes:jpeg,bmp,png|max:2000'
        ]);
    }

    public function getAllGoods($request)
    {
        /**
         * Fetch lists by user id
         */
        try {
            $goods = UGood::getGoods();

            $maped_goods = [];
            foreach ($goods as $good){
                $maped_goods[] = ParseGoodModelToGooMapper::map($good);
            }
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }

        return $maped_goods;
    }

    public function store($request,$user){
        if( count($this->validator($request)->messages()) ){
            throw new InvalidUserRegistrationException($this->validator($request)->messages());
        }else{

            $request['photo'] = NULL;
            if(array_key_exists (  'file', $request )){
                if(empty($request['photo_description'])){
                    throw new InvalidUserRegistrationException('Description cannot be empty');
                }
                $request['photo'] = $this->uploadAvatar($request['file']);
            }

            return $this->create($request,$user->apikeyable_id);
        }
    }

    public function update($id, $data){

        $good = UGood::find($id);

        if(!$good) throw new NotFoundException();

        if(array_key_exists (  'file', $data )){
            if(empty($data['photo_description'])){
                throw new InvalidUserRegistrationException('Description cannot be empty');
            }
            $data['photo'] = $this->uploadAvatar($data['file']);
        }

        return $good->update($data);

    }

    public function getGood($id)
    {
        /**
         * Fetch lists by user id and list id
         */
        try {
            $good = UGood::getGood($id);
            $maped = ParseGoodModelToGooMapper::map($good);
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }

        /**
         * if no records found throw exception
         */
        if (is_null($good)){
            throw new NotFoundException();
        }


        return $maped;
    }

    public function getByTag($id)
    {
        /**
         * Fetch lists by user id and list id
         */
        try {
            $category = UTag::getByTag($id);
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }

        /**
         * if no records found throw exception
         */
        if (is_null($category)){
            throw new NotFoundException();
        }

        return $category;
    }

    public function delete($id)
    {
        /**
         * Fetch salon by user id and list id
         */
        try {
            $good = UGood::where('id' , $id)
                ->first();
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
        /**
         * if not records found throw exception
         */
        if (is_null($good)){
            throw new NotFoundException();
        }

        /**
         * Delete list from db
         */
        try{
            DB::transaction(function () use ($good) {
                $good->tags()->delete();
                $good->delete();
            });
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }

        return true;
    }

    public function uploadAvatar($photo){
        $filename = $photo->store('public');
        $file_store_name = '/storage/'. substr($filename, 7, 1000);

        return $file_store_name;
    }
}