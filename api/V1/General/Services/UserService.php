<?php
namespace Api\V1\General\Services;

/**
 * File UserService.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\General\Services
 * @su1bpackage UserService.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\Exceptions\NotFoundException;
use Api\Common\Exceptions\UserNotFoundException;
use Api\V1\General\Entities\NotificationEntity;
use Api\V1\General\Entities\SiteEntity;
use Api\V1\General\Mappers\ParseIntegrationModelToIntegrationMapper;
use Api\V1\General\Mappers\ParseNotificationModelToNotificationMapper;
use Api\V1\General\Mappers\ParseOptinModelToOptinMapper;
use Api\V1\General\Mappers\ParsePackageModelToPackageMapper;
use Api\V1\General\Mappers\ParseSiteModelToSiteMapper;
use Api\V1\General\Mappers\ParseUserModelToUserMapper;
use Api\V1\SmartMailer\Exceptions\InvalidDataException;
use App\Integrations\Integration;
use App\Optins\Optin;
use App\Users\User;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Integrations\MarketHero;

/**
 * Class UserService
 *
 * Perform business operations for User
 *
 * @package    Api\V1\General\Services;
 * @subpackage UserService
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class UserService extends Service
{
    /**
     * @param $request
     *
     * @throws InvalidDataException
     * @throws NotFoundException
     *
     * @author     Gaik Akopian <gaikakopian94@gmail.com>
     *
     * @return array
     */
    public function auth($request){

        // Check if username or email is provided in request
        $field = filter_var($request->input('login'), FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        $request->merge([$field => $request->input('login')]);

        // Validate login and password credentials
        $validator = Validator::make($request->all(), [
            'login' => 'required|max:255',
            'password' => 'required',
        ]);

        // If validation fails throw an exception
        if ($validator->fails()){
            throw new InvalidDataException($validator->errors());
        }

        // Get user with email/username
        $user = User::where($field , $request->login)->first();

        // If user not found throw an exception
        if (!$user) {
            throw new NotFoundException();
        }

        // If user found but password from request does not match the one in db throw an exception
        if (!Hash::check($request->password, $user->getAuthPassword())) {
            throw new NotFoundException();
        }

        // user account
        $userAccount = $user->account;
        // user packages
        $packages = $userAccount->packages;

        // package table flelds list
        $limitFields = Schema::getColumnListing('packages');


        $limitEntityList = [];

        // get sum of package limits for each limit option
        foreach ($limitFields as $field){

            // if the field is not a _limit field continue
            if (!$this->stringEndsWith($field , '_limit')){
                continue;
            }

            // get all user packages limit for a _limit field
            $limitEntityList[$field] = $userAccount->limitsFromPackages($field);
        }

        $packageEntityList = [];

        // get all active user packages
        foreach ($packages as $package){

            // if package is not active continue
            if (!$package->status)
                continue;

            $packageEntityList[] = ParsePackageModelToPackageMapper::map($package);
        }

        return [
            'authenticated' => true,
            'user' => ParseUserModelToUserMapper::map($user),
            'packages' => $packageEntityList,
            'limits' => $limitEntityList
        ];

    }

    /**
     * @param $request
     *
     * @throws NotFoundException
     *
     * @author     Gaik Akopian <gaikakopian94@gmail.com>
     *
     * @return SiteEntity
     */
    public function sites($request){

        // get user by id
        $user = User::find($request->id);

        // if user not found throw an exception
        if (!$user){
            throw new NotFoundException();
        }

        // get user's sites
        $sites = $user->sites;

        // if no sites found
        if (!count($sites)){
            throw new NotFoundException();
        }

        $siteEntityList = [];
        foreach ($sites as $site){
            $siteEntityList[] = ParseSiteModelToSiteMapper::map($site);
        }

        return $siteEntityList;
    }

    /**
     * @param $request
     *
     * @author     Gaik Akopian <gaikakopian94@gmail.com>
     *
     * @throws NotFoundException
     *
     * @return NotificationEntity
     */
    public function notifications($request){

        // get user by id
        $user = User::find($request->id);

        // if user not found throw an exception
        if (!$user){
            throw new NotFoundException();
        }

        // get uesr's notifications
        $notifications = DatabaseNotification::where('data','LIKE','%"account_id":'.$user->account_id."}%")
            ->orderBy('created_at', 'desc')
            ->get();

        // if no notifications found
        if (!count($notifications)){
            throw new NotFoundException();
        }

        // get user's unread notifications count
        $unread_count = $user->unreadNotifications->count();

        $notificationEnityList['Total Unread'] = $unread_count;
        foreach ($notifications as $notification){
            $notificationEnityList[] = ParseNotificationModelToNotificationMapper::map($notification);
        }

        return $notificationEnityList;
    }

    /**
     * @param $request
     *
     * @throws InvalidDataException
     * @throws NotFoundException
     * @throws UserNotFoundException
     *
     * @author     Gaik Akopian <gaikakopian94@gmail.com>
     *
     * @return array
     */
    public function optins($request) {

        // user Id from request
        $userId = $request->user_id;

        // get user by id
        $user = User::find($userId);

        // if user does not exist throw an exception
        if (!$user) {
            throw new UserNotFoundException();
        }

        $siteId = $request->site_id;

        if ($siteId){
            // site Ids that belongs to user
            $userSites = $user->sites()->pluck('sites.id')->toArray();

            if (!in_array($siteId , $userSites)) {
                throw new InvalidDataException();
            }

        }

        // optins query
        $optinsQuery = Optin::where(['account_id' => $user->account_id]);

        // if side Id exists in request add where clause
        if ($siteId) {
            $optinsQuery->where(['site_id' => $siteId]);
        }

        // fetch optins
        $optins = $optinsQuery->orderBy('created_at' , 'desc')->get();

        // if no records found throw exception
        if (!count($optins)) {
            throw new NotFoundException();
        }

        // parse optins to OptinEntity
        $optinEntityList = [];
        foreach ($optins as $optin){
            $optinEntityList[] = ParseOptinModelToOptinMapper::map($optin);
        }

        return $optinEntityList;

    }

    /**
     * Check for substring to be in the string's end
     *
     * @param $haystack
     * @param $needle
     *
     * @author     Gaik Akopian <gaikakopian94@gmail.com>
     *
     * @return bool
     */
    protected function stringEndsWith($haystack, $needle)
    {
        $length = strlen($needle);

        return $length === 0 ||  (substr($haystack, -$length) === $needle);

    }


}