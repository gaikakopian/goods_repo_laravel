<?php
namespace Api\V1\General\Services;

/**
 * File SalonService.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\General\Services
 * @su1bpackage SalonService.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\V1\General\Exceptions\InvalidUserRegistrationException;
use Api\V1\General\Models\SMaster;
use Api\V1\General\Models\UCategory;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Api\Common\Exceptions\NotFoundException;
use Api\V1\General\Models\SSalon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

/**
 * Class SalonService
 *
 * Perform business operations for User
 *
 * @package    Api\V1\General\Services;
 * @subpackage SalonService
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class CategoriesService extends Service
{
    use AuthenticatesUsers,ValidatesRequests;

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Salons
     */
    protected function create(array $data)
    {
        $category = UCategory::create([
            'title' => $data['title'],
            'description' => $data['description']
        ]);
        return $category;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'title' => 'required|string|max:255'
        ]);
    }

    public function getAllCategories($request)
    {
        /**
         * Fetch lists by user id
         */
        try {
            $categories = UCategory::all();
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }

        return $categories;
    }

    public function store($request){

        if( count($this->validator($request)->messages()) ){
            throw new InvalidUserRegistrationException($this->validator($request)->messages());
        }else{
            return $this->create($request);
        }
    }

    public function update($id, $data){

        $category = UCategory::find($id);

        if(!$category) throw new NotFoundException();

        return $category->update($data);

    }

    public function getCategory($id)
    {
        /**
         * Fetch lists by user id and list id
         */
        try {
            $category = UCategory::find($id);
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }

        /**
         * if no records found throw exception
         */
        if (is_null($category)){
            throw new NotFoundException();
        }

        return $category;
    }

    public function delete($id)
    {
        /**
         * Fetch salon by user id and list id
         */
        try {
            $category  = UCategory::where('id' , $id)
                ->first();
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
        /**
         * if not records found throw exception
         */
        if (is_null($category)){
            throw new NotFoundException();
        }

        /**
         * Delete list from db
         */
        try{
            DB::transaction(function () use ($category) {
                $category->delete();
            });
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }

        return true;
    }
}