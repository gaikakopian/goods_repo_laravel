<?php
namespace Api\V1\General\Exceptions;

/**
 * File InvalidDataException.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\SmartMailer\Exceptions
 * @subpackage InvalidDataException.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\Exceptions\Exception;

/**
 * Class InvalidDataException
 *
 * @package   Api\V1\SmartMailer\Exceptions;
 * @subpackage InvalidDataException
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class InvalidDataException extends Exception
{

}