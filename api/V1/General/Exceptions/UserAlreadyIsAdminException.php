<?php namespace Api\V1\General\Exceptions;

/**
 * File UserAlreadyIsAdminException.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\SmartMailer\Exceptions
 * @subpackage UserAlreadyIsAdminException.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\Exceptions\Exception;

/**
 * Class UserAlreadyIsAdminException
 *
 * Generated when User cannot be created
 *
 * @package    Api\V1\SmartMailer\Exceptions;
 * @subpackage UserAlreadyIsAdminException
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class UserAlreadyIsAdminException extends Exception
{

}