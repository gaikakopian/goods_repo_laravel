<?php namespace Api\V1\General\Errors;

/**
 * File InvalidUserSubscriptionError.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\SmartMailer\Errors
 * @subpackage InvalidUserSubscription.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

/**
 * Class InvalidUserSubscriptionError
 *
 * Generate additional message when InvalidUserSubscriptionException is thrown
 *
 * @package    Api\V1\SmartMailer\Errors;
 * @subpackage InvalidUserSubscriptionError
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class InvalidUserSubscriptionError extends ValidationError
{
    /**
     * @const int
     */
    const CODE = 1001;

    /**
     * @const string
     */
    const MESSAGE = 'Invalid User Subscription';
}