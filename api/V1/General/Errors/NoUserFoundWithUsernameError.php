<?php namespace Api\V1\General\Errors;

/**
 * File NoUserFoundWithUsernameError.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\SmartMailer\Errors
 * @subpackage NoUserFoundWithUsernameError.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\Errors\Error;

/**
 * Class NoUserFoundWithUsernameError
 *
 * Generate additional message when CannotCreateUserException is thrown
 *
 * @package    Api\V1\SmartMailer\Errors;
 * @subpackage NoUserFoundWithUsernameError
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class NoUserFoundWithUsernameError extends Error
{
    /**
     * @const int
     */
    const CODE = 1003;

    /**
     * @const string
     */
    const MESSAGE = 'Cannot Find User: ';
}