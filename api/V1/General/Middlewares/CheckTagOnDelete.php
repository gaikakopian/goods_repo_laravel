<?php
namespace Api\V1\General\Middlewares;

/**
 * File CheckSalonExists.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\SmartMailer\Middlewares
 * @subpackage CheckSalonExists.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\Exceptions\Exception;
use Api\Common\Exceptions\NotFoundException;
use Api\Common\Response;
use Api\V1\General\Models\SMaster;
use Api\V1\General\Models\UCategory;
use Api\V1\General\Models\UGood;
use Api\V1\General\Models\UGoodTags;
use Api\V1\General\Models\UTag;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response as HttpResponse;
use Illuminate\Support\Facades\Validator;
use Api\V1\General\Models\SSalon;
use Api\V1\General\Exceptions\InvalidDataException;

/**
 * Class CheckSalonExists.php
 *
 * @package   Api\V1\SmartMailer\Middlewares;
 * @subpackage CheckSalonExists.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class CheckTagOnDelete
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $res = new Response(null, null, null);

        try {

            $good_tag = UGoodTags::find($request->id);

            if(!$good_tag){
                throw new NotFoundException();
            }

            $good = UGood::where('id',$good_tag['good_id'])
                        ->where('user_id',$request->apiKey->apikeyable_id)
                        ->first();

            if(!$good){
                throw new NotFoundException();
            }

        } catch (Exception $e) {
            $result = $res->setErrorFromException(1110, $e);

            return HttpResponse::json($result, 500);
        }

        return $next($request);
    }

}